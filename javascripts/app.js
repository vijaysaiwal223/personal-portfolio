const elementTime = document.getElementById("time");

function printTime() {
  date = new Date();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();

  if (seconds < 10) {
    seconds = "0" + seconds;
  }
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  if (hours > 12) {
    hours = hours - 12;
    elementTime.innerHTML = hours + " : " + minutes + " : " + seconds + "  PM ";
  } else if (hours < 12) {
    elementTime.innerHTML = hours + " : " + minutes + " : " + seconds + "  AM ";
  } else if ((hours = 12)) {
    elementTime.innerHTML = hours + " : " + minutes + " : " + seconds + "  PM ";
  }
}

setInterval(function () {
  printTime();
}, 1000);

TweenMax.from("#t1", 1, {
  delay: 0.4,
  y: 10,
  opacity: 0,
  ease: Expo.easeInOut,
});
TweenMax.from("#t2", 1, {
  delay: 0.6,
  y: 14,
  opacity: 0,
  ease: Expo.easeInOut,
});

window.onload = function () {
  //   console.log(j);
  //hide the preloader
  document.querySelector("#loder").style.display = "none";
};

TweenMax.from(".intro", 1, {
  delay: 0.8,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".cta", 1, {
  delay: 1,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".divider", 1, {
  delay: 1,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#c1", 1, {
  delay: 1.2,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#c2", 1, {
  delay: 1.4,
  y: 18,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#c3", 1, {
  delay: 1.6,
  y: 20,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#l1", 1, {
  delay: 0.4,
  y: 10,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#l2", 1, {
  delay: 0.6,
  y: 12,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#l3", 1, {
  delay: 0.8,
  y: 13,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from("#l4", 1, {
  delay: 1,
  y: 14,
  opacity: 0,
  ease: Expo.easeInOut,
});

// About Page GSAP

TweenMax.from(".about-intro", 1, {
  delay: 0.4,
  y: 14,
  opacity: 0,
  ease: Expo.easeInOut,
});
TweenMax.from(".about-detail", 1, {
  delay: 0.6,
  y: 14,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".process", 1, {
  delay: 0.8,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

TweenMax.from(".seperator", 1, {
  delay: 0.6,
  y: 16,
  opacity: 0,
  ease: Expo.easeInOut,
});

$(document).ready(function () {
  TweenMax.set(".project-preview", { width: 0 });

  var tl = new TimelineLite();

  $(document)
    .on("mouseover", ".project-item", function (evt) {
      tl = new TimelineLite();
      tl.to($(".project-preview"), 1, {
        width: "350px",
        ease: Expo.easeInOut,
      });
    })
    .on("mouseout", ".project-item", function (evt) {
      tl = new TimelineLite();
      tl.to($(".project-preview"), 0.5, {
        width: 0,
        ease: Expo.easeInOut,
      });
    });
});

$(".project-link-1").hover(function () {
  $(".project-preview").css({ "background-image": "url(/Assets/img-2.jpg)" });
});

$(".project-link-2").hover(function () {
  $(".project-preview").css({ "background-image": "url(/Assets/img-1.jpg)" });
});

$(".project-link-3").hover(function () {
  $(".project-preview").css({ "background-image": "url(/Assets/img-3.png)" });
});
$(window).scroll(function () {
  var scroll = $(window).scrollTop(),
    dh = $(document).height(),
    wh = $(window).height();
  scrollPercent = (scroll / (dh - wh)) * 100;
  $(".progressbar").css("height", scrollPercent + "%");
});
